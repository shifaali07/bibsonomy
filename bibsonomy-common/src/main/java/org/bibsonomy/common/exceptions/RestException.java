/**
 * BibSonomy-Common - Common things (e.g., exceptions, enums, utils, etc.)
 *
 * Copyright (C) 2006 - 2021 Data Science Chair,
 *                               University of Würzburg, Germany
 *                               https://www.informatik.uni-wuerzburg.de/datascience/home/
 *                           Information Processing and Analytics Group,
 *                               Humboldt-Universität zu Berlin, Germany
 *                               https://www.ibi.hu-berlin.de/en/research/Information-processing/
 *                           Knowledge & Data Engineering Group,
 *                               University of Kassel, Germany
 *                               https://www.kde.cs.uni-kassel.de/
 *                           L3S Research Center,
 *                               Leibniz University Hannover, Germany
 *                               https://www.l3s.de/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.bibsonomy.common.exceptions;

/**
 * Exception that can be used to provide error message and response statuscode information to the frontend.
 * 
 * @author Jens Illig
 */
public class RestException extends RuntimeException {
	private static final long serialVersionUID = 7907882646866488962L;
	private final int httpCode;
	private final String messageKey;
	private final String message;
	
	/**
	 * Construct
	 * @param httpCode
	 * @param message
	 * @param messageKey
	 */
	public RestException(int httpCode, String message, String messageKey) {
		this.httpCode = httpCode;
		this.message = message;
		this.messageKey = messageKey;
	}

	/**
	 * @return the httpCode
	 */
	public int getHttpCode() {
		return this.httpCode;
	}

	/**
	 * @return the messageKey
	 */
	public String getMessageKey() {
		return this.messageKey;
	}

	/**
	 * @return the message
	 */
	@Override
	public String getMessage() {
		return this.message;
	}
	
	
}
