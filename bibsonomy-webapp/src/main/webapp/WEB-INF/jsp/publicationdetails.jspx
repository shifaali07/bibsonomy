<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:jspx="urn:jsptld:/WEB-INF/taglibs/jspx.tld"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:bib="urn:jsptagdir:/WEB-INF/tags/resources/bibtex"
	xmlns:users="urn:jsptagdir:/WEB-INF/tags/users"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:pub="urn:jsptagdir:/WEB-INF/tags/resources/publication"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:resource="urn:jsptagdir:/WEB-INF/tags/resources"
	xmlns:common="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:rc="urn:jsptagdir:/WEB-INF/tags/resources/common"
	xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/resources/actionbuttons"
	xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
	xmlns:editpub="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication"
	xmlns:spring="http://www.springframework.org/tags"
	xmlns:fontawsome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
>
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	<!--+
		| variables, messages
		+-->
	<c:set var="post" value="${command.bibtex.list[0]}" />
	<c:set var="publication" value="${post.resource}" />
	<c:set var="title" value="${mtl:cleanBibtex(publication.title)}" />
	<c:set var="isOwnPost" value="${post.user.name eq command.context.loginUser.name}"/>
	
	<layout:paneLayout
			pageTitle="${title}"
			command="${command}"
			requestedUser="${command.requestedUser}"
			requPath="${requPath}"
			showPageOptions="${true}">
		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.publication" formAction="/search" formInputName="search" formInputValue="${title}"/>
		</jsp:attribute>
		
		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/publicationdetails.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/publication.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/additionalUrl.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/quickcite.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<jsp:attribute name="breadcrumbs">
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<nav:usercrumb requestedUser="${command.requestedUser}" loginUser="${command.context.loginUser}" />
					<nav:crumb name="${mtl:shorten(title, 25)}" active="true" />
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>
		
		<!--+
			| sidebar
			+-->
		<jsp:attribute name="sidebar">

			<!-- Quick cite -->
			<sidebar:sidebarItem textKey="publications.citations.headline" faIcon="quote-right">
				<jsp:attribute name="content">
					<sidebar:quickcite post="${post}" />
				</jsp:attribute>
			</sidebar:sidebarItem>

			<!-- Search on... -->
			<sidebar:sidebarItem textKey="resources.searchExternal.dropDownLabel" faIcon="search">
				<jsp:attribute name="content">
					<pub:externalSearchPublication loggedinUser="${command.context.loginUser}" publication="${publication}" buttonGroupClass="sidebar-links" />
				</jsp:attribute>
			</sidebar:sidebarItem>

			<!-- Meta data... -->
			<sidebar:sidebarItem textKey="bibtex.details.meta" faIcon="align-justify">
				<jsp:attribute name="content">
					<ul>
						<li><fmt:message key="post.change_date" /> <rc:metaChangeDate post="${post}"/><br /></li>
						<li><fmt:message key="post.date" /> <rc:metaDate post="${post}" withoutClock="${true}"/><br /></li>
					</ul>
				</jsp:attribute>
			</sidebar:sidebarItem>

			<c:set var="tagsString" value="${mtl:toTagString(post.tags)}" />
			<!-- edit tags -->
			<c:if test="${isOwnPost}">
				<sidebar:sidebarItem textKey="navi.editTags" faIcon="tags">
					<jsp:attribute name="content">
						<form action="/batchEdit?action=2" method="POST" class="sidebar_collapse_content">
							<div class="input-group">
								<input class="form-control input-sm rename-tags" type="text" name="posts['${fn:escapeXml(publication.intraHash)}_${fn:escapeXml(post.user.name)}'].newTags" value="${fn:escapeXml(tagsString)}"/>
								<fmt:message key="bibtex.actions.tags.update" var="updateTags"/>
								<span class="input-group-btn">
									<bsform:button style="defaultStyle" value="" size="small" type="submit" icon="pencil" className="rename-tags-btn" title="${updateTags}" srText="${updateTags}" />
									<button class="btn btn-sm btn-default rename-tags-submit-btn" style="display: none;" title="${updateTags}" type="button"><span class="fa fa-check"><!-- KEEP ME --></span><span class="sr-only">OK</span></button>
								</span>
							</div>
							<input type="hidden" name="posts['${fn:escapeXml(publication.intraHash)}_${fn:escapeXml(post.user.name)}'].oldTags" value="${fn:escapeXml(tagsString)}"/>
							<input type="hidden" name="posts['${fn:escapeXml(publication.intraHash)}_${fn:escapeXml(post.user.name)}'].checked" value="true" checked="checked" />
							<input type="hidden" name="referer" value="/bibtex/2${publication.intraHash}/${mtl:encodePathSegment(post.user.name)}"/>
							<input type="hidden" name="ckey" value="${fn:escapeXml(command.context.ckey)}"/>
							<input type="hidden" name="resourcetype" value="bibtex"/>
							<input type="hidden" name="updateExistingPost" value="true"/>
						</form>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>

			<!--+
				| private note
				+-->
			<c:if test="${isOwnPost}">
				<sidebar:sidebarItem textKey="post.resource.privnote" faIcon="pencil">
					<jsp:attribute name="content">
						<div id="privnote">
							<form method="post" id="note" role="form">
								<div class="form-group">
									<input type="hidden" name="ckey" value="${fn:escapeXml(command.context.ckey)}"/>
									<input type="hidden" name="intraHash" value="${fn:escapeXml(publication.intraHash)}"/>
									<input type="hidden" id="old-private-note" name="oldprivatenote" value="${fn:escapeXml(publication.privnote)}"/>
									<input type="hidden" name="action" value="updatePrivateNote"/>
									
									<textarea class="form-control" id="private-note" name="privateNote" rows="5"><c:out value="${publication.privnote}"/></textarea>
									
								</div>
								<div class="pull-right">
									<fmt:message key="save" var="save" />
									<bsform:button style="primary" value="${save}" size="xsmall" onclick="javascript: return updatePrivNote($(this))" id="makeP" />
								</div>
							</form>
						</div>
					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>

			<!--+
			| discussion and rating
			+-->
			<spring:eval var="interHashId" expression="T(org.bibsonomy.common.enums.HashID).INTER_HASH.id" />
			<pub:discussionrating post="${post}" otherPostsUrlPrefix="/bibtex/${interHashId}"/>
		</jsp:attribute>

		<jsp:attribute name="content">
			<resource:publicationdetailsclean post="${post}" referer="${command.referer}" />
		</jsp:attribute>
		
		<jsp:attribute name="customPageOptions">
			<buttons:actions post="${post}" loginUserName="${command.context.loginUser.name}" disableResourceLinks="${false}" resourceType="bibtex"/>
		</jsp:attribute>
		
	</layout:paneLayout>

</jsp:root>