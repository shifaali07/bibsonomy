<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:tags="urn:jsptagdir:/WEB-INF/tags/tags"
	xmlns:user="urn:jsptagdir:/WEB-INF/tags/resources/user"
	xmlns:users="urn:jsptagdir:/WEB-INF/tags/users"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld" 
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:review="urn:jsptagdir:/WEB-INF/tags/resources/discussion/review"
	xmlns:sidebar="urn:jsptagdir:/WEB-INF/tags/layout/sidebar"
	xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav">
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />
	
	<fmt:message key="navi.discussedPosts" var="pageTitle" />
	<layout:resourceLayout 
		pageTitle="${pageTitle}" 
		command="${command}" 
		requPath="${requPath}"
		showPageOptions="${false}"
		activeTab="${(command.requestedUser eq command.context.loginUser.name) ? 'my' : ''}">
	
		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.discussedPosts">
				<jsp:attribute name="pathPart">
					<c:if test="${not empty command.requestedUser}">
						<a href="${relativeUrlGenerator.getUserUrlByUserName(command.requestedUser)}"><c:out value='${command.requestedUser}'/></a>
					</c:if>
				</jsp:attribute>
			</parts:search>	
		</jsp:attribute>

		<jsp:attribute name="breadcrumbs">
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<c:choose>
						<c:when test="${not empty command.requestedGroup}">
							<nav:groupcrumb requestedGroup="${command.requestedGroup}" />
						</c:when>
						<c:when test="${not empty command.requestedUser}">
							<nav:usercrumb requestedUser="${command.requestedUser}" loginUser="${command.context.loginUser}" />
						</c:when>
						<c:otherwise>
							<nav:popular />
						</c:otherwise>
					</c:choose>
					<fmt:message var="posts" key="navi.discussedPosts" />
					<nav:crumb name="${posts}" active="true" />
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>

		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/merged/discussion.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<!--+
			| infobox
			+-->
		<jsp:attribute name="infobox">
			<parts:infobox>
				<jsp:attribute name="infoTextBody">
					<c:choose>
						<c:when test="${not empty command.requestedUser}">
							<fmt:message key="system.recentlyDiscussed.intro.requUser">
								<fmt:param value="${fn:escapeXml(command.requestedUser)}"/>
							</fmt:message>
						</c:when>
						<c:when test ="${not empty command.requestedGroup}">
							<fmt:message key="system.recentlyDiscussed.intro.requGroup">
								<fmt:param value="${fn:escapeXml(command.requestedGroup)}"/>
							</fmt:message>
						</c:when>
						<c:otherwise>
							<fmt:message key="system.recentlyDiscussed.intro.noUser"/>
						</c:otherwise>
					</c:choose>
					<br/>
					<fmt:message key ="system.recentlyDiscussed.intro.help"/>
				</jsp:attribute>
			</parts:infobox>
		</jsp:attribute>
		
		<!--+
		 	| sidebar
		 	+-->
		<jsp:attribute name="sidebar">
			<c:if test="${not empty command.requestedUser or not empty command.requestedGroup or not empty command.discussionsStatistic}" >
		
				<sidebar:sidebarItem textKey="post.resource.discussion.headline">
					<jsp:attribute name="content">
						<!-- TODO: create own tag for this -->
						<c:if test="${not empty command.discussionsStatistic}">
							<c:set var="stats" value="${command.discussionsStatistic}"/>
							<div class="sidebarcontainer">
								<div id="review_info_rating">
									<c:set var="ratingDistData" value="[" />
									<c:forEach var="ratings" items="${stats.values}" varStatus="status">
										<c:set var="ratingDistData" value='${ratingDistData}{"rating" : ${ratings.rating}, "count" : ${ratings.count}}' />
										<c:if test="${not status.last}">
											<c:set var="ratingDistData" value='${ratingDistData},' />
										</c:if>
									</c:forEach>
									<c:set var="ratingDistData" value="${ratingDistData}]" />
									<div id="ratingDistribution" data-distribution="${fn:escapeXml(ratingDistData)}">
										<fmt:message key="post.resource.review.info.ratingDistribution" />
										<canvas id="rating-distribution" width="265" height="60">
											<!-- ploted by JavaScript -->
										</canvas>
									</div>
									<div id="ratingAvg">
										<span><fmt:message key="post.resource.review.info.ratingAvg" /></span>
										
										<div class="rating-overview">
											<input id="averageRating" type="number" min="0" max="5" step="0.1" />
										
											<!-- plural -->
											<fmt:message var="ratingTitle" key="post.resource.review.reviews" />
											<c:if test="${stats.count == 1}">
												<!-- singular -->
												<fmt:message var="ratingTitle" key="post.resource.review.review" />
											</c:if>
						
											<fmt:formatNumber var="roundedRating" value="${stats.average}" maxFractionDigits="1" minFractionDigits="1" />
						
											<div class="ratingAvgInfo">
												<span property="ratingAverage"><c:out value="${roundedRating}" /></span>
												<c:out value=" " />
												<fmt:message key="post.resource.review.ratings.of" />
												<c:out value=" " />
												<span id="ratingBest">5.0</span><c:out value=" " /> <fmt:message key="post.resource.review.ratings.basedOn" />
												<c:out value=" " /><span property="ratingCount"><c:out value="${stats.count}" /></span>
												<c:out value=" " />
												<span>
													<c:out value="${ratingTitle}" />
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</c:if>

						<!-- link to user page -->
						<c:if test="${not empty command.requestedUser}">
							<a href="${relativeUrlGenerator.getUserUrlByUserName(fn:escapeXml(command.requestedUser))}">
								<fmt:message key="discussion.showUser">
									<fmt:param value="${fn:escapeXml(command.requestedUser)}"/>
								</fmt:message>
							</a>
						</c:if>

						<!-- link to user page -->
						<c:if test="${not empty command.requestedGroup}">
							<a href="${relativeUrlGenerator.getGroupUrlByGroupName(fn:escapeXml(command.requestedGroup))}">
								<fmt:message key="discussion.showGroup">
									<fmt:param value="${fn:escapeXml(command.requestedGroup)}"/>
								</fmt:message>
							</a>
						</c:if>

					</jsp:attribute>
				</sidebar:sidebarItem>
			</c:if>
		</jsp:attribute>
		
	</layout:resourceLayout>

</jsp:root>