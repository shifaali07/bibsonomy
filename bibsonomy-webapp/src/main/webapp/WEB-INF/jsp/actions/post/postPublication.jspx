<?xml version="1.0" ?>
<jsp:root version="2.0"
	xmlns="http://www.w3.org/1999/xhtml"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:editpub="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication"
	xmlns:fontawesome="urn:jsptagdir:/WEB-INF/tags/layout/fontawesome"
	xmlns:system="urn:jsptagdir:/WEB-INF/tags/system"
	xmlns:errors="urn:jsptagdir:/WEB-INF/tags/errors">
	
	<jsp:directive.page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" session="true" />	
	<fmt:message key="post_bibtex.heading" var="msgPostPublication"/>
	
	<layout:layout 
		loginUser="${command.context.loginUser}" 
		pageTitle="${pageTitle}"
		noSidebar="${true}"
		requPath="${requPath}">
		
		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/typeahead.js/js/typeahead.bundle.js"><!-- keep me --></script>
			<link rel="stylesheet" type="text/css" href="${resdir}/typeahead.js/css/typeahead.css"/>
			
			<script type="text/javascript" src="${resdir}/javascript/postPublication.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/javascript/customEntrytypes.js"><!-- keep me --></script>
			<script type="text/javascript" src="${resdir}/handlebars/handlebars.js"><!-- keep me --></script>
		</jsp:attribute>
		
		<jsp:attribute name="content">
			<div id="post-publication" class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<div class="content-header">
						<h1><fmt:message key="navi.postPublication"/></h1>
					</div>
					<errors:global />
					
					<div class="row">
						<div class="col-sm-12">
							<p class="lead">
								<fmt:message key="post_publication.desc" />
							</p>
						</div>
					</div>
					<div class="row" id="publication-search">
						<div class="col-sm-2 text-right">
							<fontawesome:icon icon="search" size="3x"/>
						</div>
						<div class="col-sm-8">
							<fmt:message key="post_publication.search.placeholder" var="searchPlaceholder" />
							<input type="text" class="form-control input-lg" placeholder="${searchPlaceholder}" />
							<p class="search-info"><fmt:message key="post_publication.search.info"/></p>
						</div>
					</div>
					
					<p class="separator"><span><fmt:message key="post_publication.other.methods" /></span></p>
					
					<div class="row" id="publication-functions">
						<div class="col-sm-12 text-center">
							<div class="btn-group btn-group-lg">
								<button data-target="file" type="button" class="btn btn-default"><fmt:message key="post_publication.file.title" /></button>
								<button data-target="snippet" type="button" class="btn btn-default"><fmt:message key="post_bibtex.pub_snippet.snippet"/></button>
								<!-- rja, 2021-05-28, disabled, since flash is dead;
  					                             needs to be re-implemented using HTML5
								<button data-target="scan" type="button" class="btn btn-default"><fmt:message key="post_bibtex.scan.title" /></button>
                                                                -->
								<button data-target="manual" type="button" class="btn btn-default"><fmt:message key="post_bibtex.manual.title" /></button>
							</div>
						</div>
					</div>
					
					<div class="row publication-function" id="publication-file">
						<div class="col-sm-8 col-sm-offset-2">
							<p>
								<fmt:message key="post_publication.file.desc" />
								<system:beta />
							</p>
							
							<form:form cssClass="form-horizontal" name="upload" method="post" enctype="multipart/form-data" action="/import/publications?ckey=${ckey}">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="file">
										<fmt:message key="post_bibtex.upload.my_file" />*
									</label>
									<div class="col-sm-10">
										<input type="file" name="file" id="file" class="reqinput fsInput" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<fmt:message var="post_button" key="post_bibtex.button_label_post" />
										<bsform:button style="primary" size="defaultSize" type="submit" value="${post_button}" />
									</div>
								</div>
							</form:form>
						</div>
					</div>
					
					<div class="row publication-function" id="publication-snippet">
						<div class="col-sm-8 col-sm-offset-2">
							<post:postPublicationSnippet />
						</div>
					</div>
					<!-- rja, 2021-05-28, disabled, since flash is dead;
  					     needs to be re-implemented using HTML5
					<div class="row publication-function" id="publication-scan">
						<div class="col-sm-8 col-sm-offset-2">
							<post:postPublicationScan />
						</div>
					</div>
                                        -->
					
					<div class="row publication-function" id="publication-manual">
						<div class="col-sm-8 col-sm-offset-2">
							<form:form cssClass="form-horizontal" id="postForm" action="/editPublication" method="post">
								<fieldset>
									<p><fmt:message key="post_bibtex.manual.description" /></p>	
									
									<input type="hidden" name="selTab" value="0" />
									<input type="hidden" name="ckey" value="${ckey}"/>
									<c:if test="${command.person != null}">
										<input type="hidden" name="personId" value="${command.person.personId}"/>
									</c:if>
									<input type="hidden" name="personRole" value="${command.personRole}"/>
									
									<editpub:required post="${post}" hideBibTeXKey="${true}" personIndex="${command.personIndex}" person="${command.person}" />
									
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-9">
											<fmt:message var="post_button" key="post_bibtex.button_label_post" />
											<bsform:button size="defaultSize" type="submit" style="primary" value="${post_button}"/>
										</div>
									</div>
								</fieldset>
							</form:form>
						</div>
					</div>
					
					<p class="separator"><span><fmt:message key="post_publication.addons.title" /></span></p>
					
					<div class="row">
						<div class="col-sm-12">
							<p class="lead">
								<fmt:message key="post_publication.addons.desc" />
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-10 col-sm-offset-1">
							<buttons:addons showBookmarkletAlternative="${false}"/>
						</div>
					</div>
				</div>
			</div>
		</jsp:attribute>
	</layout:layout>
</jsp:root>
