<jsp:root version="2.0" 
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:post="urn:jsptagdir:/WEB-INF/tags/post"
	xmlns:buttons="urn:jsptagdir:/WEB-INF/tags/buttons"
	xmlns:form="http://www.springframework.org/tags/form"
	xmlns:editpub="urn:jsptagdir:/WEB-INF/tags/actions/edit/publication"
	xmlns:bsform="urn:jsptagdir:/WEB-INF/tags/bs/form">

	<jsp:directive.attribute name="tags" type="java.lang.String" required="false"/>
	<jsp:directive.attribute name="bibtex" type="org.bibsonomy.model.BibTex" required="true"/>
	<jsp:directive.attribute name="referer" type="java.lang.String" required="true"/>

	<!--+
		|
		| FIXME: we need a check around the whole code below to ensure that it
		| is only called in PUMA. 
		|
	 	+-->
	<!--+ 
	 	|
	 	| additional JavaScript code for Open Access (DSpace) integration.
	 	|
	 	+-->
	<c:if test="${properties['publication.reporting.enabled'] eq 'true'}">
		<c:if test="${not empty referer}">
			<a class="btn btn-default" href="${referer}"><fmt:message key="post.resource.openaccess.button.referer"/></a>
		</c:if>
		
		<div style="clear:both;" id="oaRepositorySentInfo"><!-- used for puma--> </div>	
		<h4 class="pub-details"><span class="fa fa-paper-plane"><!--  --></span><c:out value=" " /><fmt:message key="post.resource.openaccess.headline"/></h4>
		<div class="oadescription">
			<p><fmt:message key="post.resource.openaccess.description"/></p>
		</div>
		<form class="form-horizontal">
			<fieldset>
				<div id="openAccessContainer">

				<div id="openAccessMetaDataContainer">
					<legend><fmt:message key="post.resource.openaccess.additionalfields.headline"/></legend>
					
					<div class="oadescription"><p><fmt:message key="post.resource.openaccess.additionalfields.description"/></p></div>
					
					<!-- if entrytype == phdthesis -->
					<c:if test="${bibtex.entrytype=='phdthesis' || bibtex.entrytype=='mastersthesis'}">
						
						<div class="form-group">
							<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.institution"><fmt:message key="post.resource.openaccess.additionalfields.institution"/></label>
							<div class="col-sm-9">
								<input type="text" value="" class="form-control help-popover" onchange="setMetadataChanged(true)" onkeyup="setMetadataChanged(true)" name="post.resource.openaccess.additionalfields.institution" id="post.resource.openaccess.additionalfields.institution"/>
								<div class="help help-header hide"><!--  --></div>
								<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.institution.help"/></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.referee"><fmt:message key="post.resource.openaccess.additionalfields.referee"/></label>
							<div class="col-sm-9">
								<input type="text" value="" class="form-control help-popover" onchange="setMetadataChanged(true)" onkeyup="setMetadataChanged(true)" name="post.resource.openaccess.additionalfields.referee" id="post.resource.openaccess.additionalfields.phdreferee"/>
								<div class="help help-header hide"><!--  --></div>
								<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.referee.help"/></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.referee2"><fmt:message key="post.resource.openaccess.additionalfields.referee"/></label>
							<div class="col-sm-9">
								<input type="text" value="" class="form-control help-popover"  name="post.resource.openaccess.additionalfields.referee2" id="post.resource.openaccess.additionalfields.phdreferee2"/>
								<div class="help help-header hide"><!--  --></div>
								<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.referee.help"/></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.phdoralexam"><fmt:message key="post.resource.openaccess.additionalfields.phdoralexam"/></label>
							<div class="col-sm-9">
								<input type="text" value="" class="form-control help-popover" onchange="setMetadataChanged(true)" onkeyup="setMetadataChanged(true)" name="post.resource.openaccess.additionalfields.phdoralexam" id="post.resource.openaccess.additionalfields.phdoralexam"/>
								<div class="help help-header hide"><!--  --></div>
								<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.phdoralexam.help"/></div>
							</div>
						</div>
					</c:if> <!-- /if entrytype == phdthesis -->
					<div class="form-group">
						<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.sponsor"><fmt:message key="post.resource.openaccess.additionalfields.sponsor"/></label>
						<div class="col-sm-9">
							<input type="text" value="" class="form-control help-popover" onchange="setMetadataChanged(true)" onkeyup="setMetadataChanged(true)" name="post.resource.openaccess.additionalfields.sponsor" id="post.resource.openaccess.additionalfields.sponsor"/>
							<div class="help help-header hide"><!--  --></div>
							<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.sponsor.help"/></div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label" for="post.resource.openaccess.additionalfields.additionaltitle"><fmt:message key="post.resource.openaccess.additionalfields.additionaltitle"/></label>
						<div class="col-sm-9">
							<input type="text" value="" class="form-control help-popover" onchange="setMetadataChanged(true)" onkeyup="setMetadataChanged(true)" name="post.resource.openaccess.additionalfields.additionaltitle" id="post.resource.openaccess.additionalfields.additionaltitle"/>
							<div class="help help-header hide"><!--  --></div>
							<div class="help help-content hide"><fmt:message key="post.resource.openaccess.additionalfields.additionaltitle.help"/></div>
						</div>
					</div>
					<div id="sendMetadataMarker">&amp;deg;</div>
									
					<!--						<div id="saveMetadataButtonContainer">-->
					<!--							<a id="saveMetadataButton" href="javascript:sendAdditionalMetadataFields()"><fmt:message key="post.resource.openaccess.additionalfields.save"/></a>-->
					<!--						</div>-->
					
						<input type="hidden" name="hash" id="openAccessCurrentPublicationHash" value="${fn:escapeXml(bibtex.intraHash)}"/>
						<input type="hidden" name="hash" id="openAccessCurrentPublicationInterHash" value="${fn:escapeXml(bibtex.interHash)}"/>
					</div>
					<div id="pumaAddon" class="clearboth" ><!-- used for puma--> </div>
			
					<div class="form-group" id="openAccessClassificationContainer">
						
						<label class="control-label col-sm-3"><fmt:message key="post.resource.openaccess.classification.headline"/></label>
						
						<div class="col-sm-9">
							<div class="oadescription"><fmt:message key="post.resource.openaccess.classification.description"/></div>
							<div id="openAccessClassificationList"><!-- openAccessClassificationListContainer --></div>
							<div id="openAccessClassificationSelect"><!-- openAccessClassificationSelectContainer --></div>
						</div>
					</div>

					<script type="text/javascript">
						$(function() {
		
							// set metadatafields
							var mdf = Array(); 
							var i=0;
							mdf[i++] = "post.resource.openaccess.additionalfields.institution";
							mdf[i++] = "post.resource.openaccess.additionalfields.phdreferee";
							mdf[i++] = "post.resource.openaccess.additionalfields.phdreferee2";
							mdf[i++] = "post.resource.openaccess.additionalfields.phdoralexam";
							mdf[i++] = "post.resource.openaccess.additionalfields.sponsor";
							mdf[i++] = "post.resource.openaccess.additionalfields.additionaltitle";
							setMetadatafields(mdf);
		
							setTimeout('initialiseOpenAccessClassification(&quot;openAccessClassification&quot;, &quot;${bibtex.intraHash}&quot;)', 1);
							//setTimeout('initialiseOpenAccessSendToRepository(&quot;oasendtorepositorybutton&quot;, &quot;${bibtex.intraHash}&quot;)', 1);
							setTimeout("loadStoredClassificationItems()", 1);
							setTimeout("loadAdditionalMetadataFields()", 1);
							setTimeout("checkOpenAccess()", 1);
							setTimeout("loadSentRepositories()", 1);
							
							setTimeout("setBackgroundColor(\"referer\", \"#EEEEEE\")", 1000);
							setTimeout("setBackgroundColor(\"referer\", \"#DDDDDD\")", 2000);
							setTimeout("setBackgroundColor(\"referer\", \"#EEEEEE\")", 3000);
							setTimeout("setBackgroundColor(\"referer\", \"#DDDDDD\")", 4000);
							setTimeout("setBackgroundColor(\"referer\", \"#EEEEEE\")", 5000);
							setTimeout("setBackgroundColor(\"referer\", \"#DDDDDD\")", 6000);
					    });
					</script>

					<!-- a style="cursor: pointer;" onclick="javascript:checkOpenAccess();"><fmt:message key="openaccess.check"/></a> -->
	
					<c:if test="${((bibtex.entrytype=='article') and (!empty bibtex.journal)) or ((bibtex.entrytype!='article') and (!empty bibtex.publisher))}"> 
					<div class="form-group" id="oasherparomeocontainer">
						
						<label class="control-label col-sm-3"><fmt:message key="post.resource.openaccess.sherparomeo.headline"/></label>
						<div class="col-sm-9">
							<c:set var="oaRequestPublisherUrlParameter" value="?action=SHERPAROMEO&amp;publisher=${bibtex.publisher}"/>
							<c:if test="${bibtex.entrytype=='article'}">
								<c:set var="oaRequestPublisherUrlParameter" value="?action=SHERPAROMEO&amp;jTitle=${bibtex.journal}"/>
							</c:if>
							<input type="hidden" id="oaRequestPublisherUrlParameter" value="${oaRequestPublisherUrlParameter}"/>
							
							<div id="oasherparomeopredescription" class="oasherparomeodescription"><fmt:message key="post.resource.openaccess.sherparomeo.description"/></div>				
							
							<c:if test="${bibtex.entrytype=='article'}">
								<div id="oasherparomeopublisher"><fmt:message key="post.resource.openaccess.sherparomeo.publisher.entrytype.article"><fmt:param value="${bibtex.journal}"/></fmt:message></div>
							</c:if>
							<c:if test="${bibtex.entrytype!='article'}">
								<div id="oasherparomeopublisher"><fmt:message key="post.resource.openaccess.sherparomeo.publisher.entrytype.other"><fmt:param value="${bibtex.publisher}"/></fmt:message></div>
							</c:if>
							
							<div id="oasherparomeo"><fmt:message key="post.resource.openaccess.noinfoavailable"/></div>
							
							<div id="oasherparomeocolordescription">
								<fmt:message key="post.resource.openaccess.sherparomeo.color.description.intro"/>  
								<ul>
									<li class="oa-green"><fmt:message key="post.resource.openaccess.sherparomeo.color.description.green"/></li>
									<li class="oa-blue"><fmt:message key="post.resource.openaccess.sherparomeo.color.description.blue"/></li>
									<li class="oa-yellow"><fmt:message key="post.resource.openaccess.sherparomeo.color.description.yellow"/></li>
									<li class="oa-white"><fmt:message key="post.resource.openaccess.sherparomeo.color.description.white"/></li>
								</ul>
							</div>
						</div>
					</div>
					</c:if>
					
					<div class="form-group" id="authorcontract">
						<div class="col-sm-offset-3 col-sm-9">
							<div class="checkbox">
								<label id="authorcontractconfirmcontainer">
									<input onclick="checkauthorcontractconfirm();" onchange="checkauthorcontractconfirm();" type="checkbox" id="authorcontractconfirm"/>
									<fmt:message key="post.resource.openaccess.authorcontractconfirm"/>
								</label>
							</div>
						</div>
					</div>
					
					<div id="oaRepositorySent"><!-- used for puma--> </div>
					
					<div class="form-group" id="oaSubmitContainer" >
						<div class="col-sm-offset-3 col-sm-9" id="pumaSword">
							<fmt:message key="post.resource.openaccess.button.sendtorepository" var="oaButtonSendtorepository"/>
							<input id="oasendtorepositorybutton" type="button" class="ajaxButton oadisabledsend2repositorybutton btn btn-primary" onclick="sentPublicationToRepository('oasendtorepositorybutton','${bibtex.intraHash}')" value="${oaButtonSendtorepository}" disabled="disabled"/>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</c:if>
</jsp:root>