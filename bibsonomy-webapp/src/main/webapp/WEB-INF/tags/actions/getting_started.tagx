<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:parts="urn:jsptagdir:/WEB-INF/tags/layout/parts"
	xmlns:layout="urn:jsptagdir:/WEB-INF/tags/layout"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:mtl="urn:jsptld:/WEB-INF/taglibs/mytaglib.tld"
	xmlns:nav="urn:jsptagdir:/WEB-INF/tags/nav">
	
	<!-- TODO: (Bootstrap) rename fragment name -->
	<jsp:directive.attribute name="confirmationMailFragment" required="false" fragment="true"/>
	
	<jsp:directive.attribute name="command" type="org.bibsonomy.webapp.command.BaseCommand" required="true"/>
	
	<fmt:message key="gettingStarted" var="gettingStartedTitle" />

	<layout:paneLayout pageTitle="${gettingStartedTitle}"
		headerMessageKey="gettingStarted.headline" command="${command}"
		requPath="${requPath}" noSidebar="${true}">

		<jsp:attribute name="heading">
			<parts:search pathMessageKey="navi.search" formAction="/search" formInputName="search"/>
		</jsp:attribute>

		<jsp:attribute name="headerExt">
			<script type="text/javascript" src="${resdir}/javascript/animatescroll.js"><!-- --></script>
			<script type="text/javascript" src="${resdir}/javascript/animate-scroll-affix.js"><!-- --></script>
		</jsp:attribute>
		
		<!-- TODO: (Bootstrap) breadcrumbs
		<jsp:attribute name="breadcrumbs">	
			<nav:breadcrumbs>
				<jsp:attribute name="crumbs">
					<nav:crumb name="${gettingStartedTitle}" />
				</jsp:attribute>
			</nav:breadcrumbs>
		</jsp:attribute>
		-->
		
		<jsp:attribute name="content">
			<div class="row">
				<div class="col-md-offset-1 col-xs-12 col-sm-12 col-md-10">
					
					
					<c:if test="${not empty confirmationMailFragment}">
						<jsp:invoke fragment="confirmationMailFragment" />
						
						<hr />
					</c:if>
					
					<div class="text-center">
						<h1>
							<fmt:message key="gettingStarted.headline">
								<fmt:param value="${properties['project.name']}" />
							</fmt:message>
						</h1>
						
						<div class="lead">
							<p>
								<fmt:message key="gettingStarted.intro">
									<fmt:param value="${properties['project.name']}" />
								</fmt:message>
							</p>
						</div>
					</div>
					<hr />
					
					<div class="row">
					
						<div class="hidden-xs col-sm-3">
							<div class="affix">
								<div class="list-group section-scroll">
			 						
		 							<!-- <a class="list-group-item active" onclick="javascript: activateAffixEntry(this)" href="#overview"><fmt:message key="gettingStarted.navi.overview" /></a> -->		
	 								<a class="list-group-item active" href="#collect"><fmt:message
											key="gettingStarted.navi.collect" /></a>
	 								<a class="list-group-item" href="#cite"><fmt:message
											key="gettingStarted.navi.cite" /></a>
	 								<a class="list-group-item" href="#collaborate"><fmt:message
											key="gettingStarted.navi.collaborate" /></a>
		 							<a class="list-group-item" href="#discover"><fmt:message
											key="gettingStarted.navi.discover" /></a>
									<a class="list-group-item" href="#export"><fmt:message
											key="gettingStarted.navi.export" /></a>
								</div>
							</div>
						</div>
						
						<div class="col-sm-9">		
							<div id="collect">
								<a name="collect">
									<!--  -->
								</a>
								<h2 class="getting-started">
									<fmt:message key="gettingStarted.navi.collect" />
								</h2>
								<div class="lead text-center">
									<fmt:message key="gettingStarted.navi.collect.desc">
										<fmt:param value="${properties['project.name']}" />
									</fmt:message>
								</div>
								<div class="row">
									<div class="col-sm-6">	
										<fmt:message key="gettingStarted.collect">
											<fmt:param value="${properties['project.name']}" />
										</fmt:message>
									</div>
									<div class="col-sm-6">
										<!-- Browser addon buttons -->
										<!-- Firefox button -->
											<p class="firefox-addon">
												<a href="${properties['project.addons.firefox.url']}" class="btn btn-success btn-lg btn-block">
													<fmt:message key="bookmarklet.buttonTextFirefox" />
												</a>
											</p>
										<!-- Chrome button -->
											<p class="chrome-addon">
												<a href="${properties['project.addons.chrome.url']}" class="btn btn-success btn-lg btn-block">
													<fmt:message key="bookmarklet.buttonTextChrome" />
												</a>
											</p>
										<!-- Safari buttons -->
											<p class="safari-addon">
												<a class="btn btn-success btn-lg btn-block" href="/resources/addons/safari/${properties['project.theme']}-buttons-extension/${properties['project.theme']}_buttons.safariextz">
													<fmt:message key="bookmarklet.buttonTextSafari" />
												</a>
											</p>
										<hr></hr>
										<img class="img-thumbnail img-responsive" src="/resources/image/addons/${locale}/bookmarklet_screenshot_chrome.png" />
									</div>
								</div>
							</div>
							<hr />
							<div id="cite">
								<a name="cite">
									<!--  -->
								</a>
								<h2 class="getting-started">
									<fmt:message key="gettingStarted.navi.cite" />
								</h2>
								<div class="lead text-center">
									<fmt:message key="gettingStarted.navi.cite.desc" />
								</div>
								<div class="row">
									<div class="col-sm-6">
										<fmt:message key="gettingStarted.cite">
											<fmt:param value="${properties['project.name']}" />
										</fmt:message>
									</div>
									<div class="col-sm-6">
										<img class="img-thumbnail img-responsive"
											src="/resources/image/gettingStarted-cite.png" />
									</div>
								</div>
							</div>
							<hr />
							<div id="collaborate">
								<a name="collaborate">
									<!--  -->
								</a>
								<h2 class="getting-started">
									<fmt:message key="gettingStarted.navi.collaborate" />
								</h2>
								<div class="lead text-center">
									<fmt:message key="gettingStarted.navi.collaborate.desc" />
								</div>
								<div class="row">
									<div class="col-sm-6">
										<fmt:message key="gettingStarted.collaborate">
											<fmt:param value="${properties['project.name']}" />
										</fmt:message>
									</div>
									<div class="col-sm-6">
										<img class="img-thumbnail img-responsive" 
											src="/resources/image/gettingStarted-collaborate-${properties['project.theme']}.png" />									
									</div>
								</div>
							</div>
							<hr />
							<div id="discover">
								<a name="discover">
									<!--  -->
								</a>
								<h2 class="getting-started">
									<fmt:message key="gettingStarted.navi.discover" />
								</h2>
								<div class="lead text-center">
									<fmt:message key="gettingStarted.navi.discover.desc" />
								</div>
								<div class="row">
									<div class="col-sm-6">
										<fmt:message key="gettingStarted.discover">
											<fmt:param value="${properties['project.name']}" />
										</fmt:message>
									</div>
									<div class="col-sm-6">
										<img class="img-thumbnail img-responsive"
											src="/resources/image/gettingStarted-discover-${properties['project.theme']}.png" />
									</div>
								</div>
							</div>
							<hr />
							<div id="export">
								<a name="export">
									<!--  -->
								</a>
								<h2 class="getting-started">
									<fmt:message key="gettingStarted.navi.export" />
								</h2>
								<div class="lead text-center">
									<fmt:message key="gettingStarted.navi.export.desc">
										<fmt:param value="${properties['project.name']}" />
									</fmt:message>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<fmt:message key="gettingStarted.export">
											<fmt:param value="${properties['project.name']}" />
										</fmt:message>
									</div>
									<div class="col-sm-6">
										<img class="img-thumbnail img-responsive"
											src="/resources/image/gettingStarted-export-${properties['project.theme']}.svg" />
									</div>
								</div>
							</div>
							<p>${mtl:ch('nbsp')}</p>
						</div>
						<br />
					</div>
					
				</div>
				<!-- Browser detection -->
				<script type="text/javascript">
					$(function() {
						var availableAddons = "${properties['project.addons.available']}";
						var ua = navigator.userAgent.toLowerCase();

						console.log("Browser: " + ua);
						$("#browser-addon").hide();
						
						$(".firefox-addon").hide();
						$(".chrome-addon").hide();
						$(".safari-addon").hide();
						
						$(".addonhint").hide();
						$(".bookmarklethint").hide();
						$("#internetExplorer").hide();
						
						if (!availableAddons.length) {
							$(".bookmarklethint").show();
							return;
						}
						
						if (ua.indexOf("firefox") > -1) { //this is firefox
							showAddOn("firefox");
						} else if (ua.indexOf("opr") > -1) {
							// this is opera
						} else if (ua.indexOf("chrome") > -1) { //this is chrome
							showAddOn("chrome");
						} else if (ua.indexOf("safari") > -1) { //this is safari
							showAddOn("safari");
						} else if (ua.indexOf("MSIE") > -1) { // this is IE
							$("#internetExplorer").show();
						}
					});
					
					function showAddOn(addon) {
						$("#browser-addon").show();
						$("." + addon + "-addon").show();
						$(".addonhint").show();
					}
				</script>
				
			</div>
		</jsp:attribute>


	</layout:paneLayout>

</jsp:root>